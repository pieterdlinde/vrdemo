
# Remember to always be awesome. Being a developer in EOH is awesome

# VrTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## AFRAME

This Game/Demo was build on an Aframe .api for more information go to Aframe.io. Aframe is cool and a easy tool to use

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Remober to add you own firebase connections

src -> app -> appmodule -> firebaseConfig

## Dont forget be awesome 

This demo was based on this url -> https://www.youtube.com/watch?v=8A28iQPxTPs

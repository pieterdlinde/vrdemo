import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ResetPageComponent } from '../components/reset-page/reset-page.component';
import { GamePageComponent } from '../components/game-page/game-page.component';
import { StartPageComponent } from '../components/start-page/start-page.component';

 

const routes: Routes = [
  { path: 'home', component:  StartPageComponent},
  { path: 'resetpage', component: ResetPageComponent },	
  { path: 'game/:name', component:  GamePageComponent},
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: '**', component: GamePageComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule{ } 
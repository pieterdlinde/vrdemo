import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';

import { AframePipeModule } from 'angular-aframe-pipe';

import { AngularFireModule } from 'angularfire2'; 
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ResetPageComponent } from '../components/reset-page/reset-page.component';
import { AppRoutingModule } from './app-routing.module'; 
import { GamePageComponent } from '../components/game-page/game-page.component'; 
import { StartPageComponent } from '../components/start-page/start-page.component';
import { FormsModule } from '@angular/forms';

export const firebaseConfig = {
  apiKey: "AIzaSyCv2zh9Dr30JfZK_JVJnC0HbtNTlFC7TRU",
  authDomain: "diewithmefree-fc40f.firebaseapp.com",
  databaseURL: "https://diewithmefree-fc40f.firebaseio.com",
  projectId: "diewithmefree-fc40f",
  storageBucket: "diewithmefree-fc40f.appspot.com",
  messagingSenderId: "741248273905"
};

@NgModule({
  declarations: [
    AppComponent,
    ResetPageComponent,
    GamePageComponent,
    StartPageComponent
  ],
  imports: [
    BrowserModule,
    AframePipeModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    AppRoutingModule
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
  
})
export class AppModule { }

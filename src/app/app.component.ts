import { Component } from '@angular/core';
 
import * as screenfull from 'screenfull'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', 
  styleUrls: ['./app.component.css']
})
export class AppComponent { 
  title = "Awesome VR Game that hopefully works"
  constructor(){ 
    if (screenfull.enabled) {
      console.log("RequestFullScreen")
      screenfull.request();
    }
  }
 


}
    
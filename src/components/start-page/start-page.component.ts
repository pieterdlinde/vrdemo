import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {
public hero:any =""
  constructor(private router: Router) { }

  ngOnInit() {
  }

  startGame(){
    if(this.hero == ""){
      this.hero = "Hero"
    }

    this.router.navigate(['./game',this.hero]);

  }

}

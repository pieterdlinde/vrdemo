import { Component, OnDestroy, HostListener } from '@angular/core';

import { map } from "rxjs/operators";
// import { UserPresenceService, IUser } from './UserPresenceService';
import { AngularFireDatabase } from 'angularfire2/database';
import { AframePipe } from 'angular-aframe-pipe';
import * as AFRAME from 'aframe'
import * as screenfull from 'screenfull'

import {Router, ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.css']
})
export class GamePageComponent  implements OnDestroy {
  
  title = 'app';
  public users: any;
  public me: IUser;
  public usersRef:any;
  public randRadius = 3 + Math.random() * 8;
  public randAngle = Math.random() * Math.PI * 2;
  public baseX = Math.sin(this.randAngle) * this.randRadius;
  public baseZ = Math.cos(this.randAngle) * this.randRadius;
  public placeMats:any;
  private playerName: string = "Hero";
  public playerScore:string = "Killes: 0"
  public textValue = "value: "+this.playerScore+"; color:black;width:10;height:10;"
  public playerLives:string = "Lives: 0"
  public textValueLives = "value: "+this.playerLives+"; color:black;width:10;height:10;"
  constructor(public afDB: AngularFireDatabase
    , private router: Router
    , private route: ActivatedRoute){
      if (screenfull.enabled) {
        console.log("RequestFullScreen")
        screenfull.request();
      }

      this.route.params.subscribe(params => {
        if(params.name != null){
          this.playerName =  params.name   
        }  
      });
       
      let userColor = "green"// randomColor()
      let playerName = "value: "+this.playerName+"; color:"+userColor+";width:10;height:10;"
       
      var newUser:IUser ={
        id:"1",
        color:userColor,
        x:this.RandomNumber(),
        z:this.RandomNumber(),
        rotationX:0,
        rotationY:0,
        me: false,
        lives: 2,
        killes:0,
        name:playerName,
        pname:this.playerName

      }
      newUser.me = false
      var newUserRef = afDB.list('people').push({
        id:"1",
        color:userColor,
        x:this.RandomNumber(),
        z:this.RandomNumber(),
        rotationX:0,
        rotationY:0,
        me: false,
        lives: 2,
        killes:0,
        name:playerName,
        pname:this.playerName
      });
      
      afDB.list('players').push({
        name:this.playerName
      })

      newUser.key = newUserRef.ref.key;
      this.me = newUser 
      this.me.x = this.RandomNumber()
      this.me.z = this.RandomNumber()
      this.loadPlaceMats()
      this.getData()
       
    }
    
     RandomNumber(): number{
      let val = Math.floor((Math.random() * 10) + 1)
      console.log(val)
      if(val >= 5){
        return Math.floor((Math.random() * 30) + 1) * -1
      }else{
        return Math.floor((Math.random() * 30) + 1) 
      }
    }

    HoverMe(){
      console.log("Hovered")
    }
    
    getData(){
      var usersList = this.afDB.list('/people').snapshotChanges().pipe(map(actions => {
        return actions.map(a => {`as`
        const data = a.payload.val();
        const key = a.payload.key;
        return { key, ...data };
      });
    }));
    
    usersList.subscribe(items => {
      this.users = items  
      for (var i = 0; i < this.users.length; i++) {
        if(this.users[i].lives <= 0){ 
          this.users[i].me = false
          this.afDB.list('people').remove(this.users[i].key) 
        }
        if (this.users[i].key == this.me.key) {
          this.me  = this.users[i]
          this.users[i].me = true;
          this.playerLives = "Lives: " + this.me.lives + ""
          this.textValueLives = "value: "+this.playerLives+"; color:black;width:10;height:10;"
          if(this.me.lives <= 0){ 
            this.router.navigate(['./resetpage',this.me]);
          }
        }
      }
    })
  }
  
  
  userId(User:any){ 
    return User.key; 
  }
  
  onRotationChanged(object:any){
    // this.me.rotationX = object.x;
    if(this.me.x == 0 || this.me.z == 0){
      this.me.x =  this.RandomNumber();
      this.me.z =  this.RandomNumber();
    }
    var diff = Math.abs(this.me.rotationY - object.y)    
    if(diff > 5 ){
      this.me.rotationY = object.y ;
      this.me.me = false;
      this.afDB.list('people').update(this.me.key,{
        id:this.me.id,
        color: this.me.color,
        x: this.me.x,
        z: this.me.z, 
        rotationX: this.me.rotationX,
        rotationY: this.me.rotationY,
        me: false,
        key:this.me.key,
        lives: this.me.lives,
        killes:this.me.killes,
        name:this.me.name,
        pname:this.me.pname
      }).then(x=>{
        this.me.me = true 
      }).catch(err=>{
        console.error(err)
      })  
      this.me.me = true 
    }
  }
  
  onPositionChanged(object:any){
    
    this.me.x =  object.x
    this.me.z =   object.z
    if(this.me.x == 0 || this.me.z == 0){
      this.me.x =  this.RandomNumber();
      this.me.z =  this.RandomNumber();
    }
    this.me.me = false;
    this.afDB.list('people').update(this.me.key,{
      id:this.me.id,
      color: this.me.color,
      x: this.me.x,
      z: this.me.z, 
      rotationX: this.me.rotationX,
      rotationY: this.me.rotationY,
      me: false,
      key:this.me.key,
      lives: this.me.lives,
      killes:this.me.killes,
      name:this.me.name,
      pname:this.me.pname
    }).then(x=>{
      this.me.me = true 
    }).catch(err=>{
      console.error(err)
    })  
    this.me.me = true 
  }
  
  clickEvent(object){
    if(object.key == this.me.key){
      console.log("Self Shooter")
      return
    }
    object.color = "red"
    object.lives = object.lives - 1
    this.me.killes = this.me.killes + 1
    this.playerScore = "Score: " + this.me.killes 
    this.textValue = "value: "+this.playerScore+"; color:black;width:10;height:10;"
    this.me.me = false;
    object.me = false
    this.afDB.list('people').update(this.me.key,{
      id:this.me.id,
      color: this.me.color,
      x: this.me.x,
      z: this.me.z, 
      rotationX: this.me.rotationX,
      rotationY: this.me.rotationY,
      me: false,
      key:this.me.key,
      lives: this.me.lives,
      killes:this.me.killes,
      name:this.me.name,
      pname:this.me.pname
    }).then(x=>{
      this.me.me = true 
    }).catch(err=>{
      console.error(err)
    }) 
    this.me.me = true 
    this.afDB.list('people').update(object.key,object)  
     
  }
  
  moveMe(obj){ 
    this.onPositionChanged(obj)
  } 
  
  ngOnDestroy() { 
    
  }
  
  
  
  loadPlaceMats(){
    this.placeMats = []
    for (var x = -30; x < 30; x = x + 2) {
      for (var z = -30; z < 30; z = z + 2) {
        this.placeMats.push({x:x,z:z})
      }
    }
    
    
  }
  
}
// const randomColor = () =>
// '#' +
// Math.round(0x1000000 + 0xffffff * Math.random())
// .toString(16)
// .slice(1);

export interface IUser {
  id: string;
  color: string;
  x: number;
  z: number;
  rotationX?: number;
  rotationY?: number;
  me?: boolean;
  key?:string;
  lives?: number;
  killes?: number;
  name?: string;
  pname?:string;
}


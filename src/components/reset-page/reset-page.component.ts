import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reset-page',
  templateUrl: './reset-page.component.html',
  styleUrls: ['./reset-page.component.css']
})
export class ResetPageComponent implements OnInit {
public score:string = "0"
public hero:any =""
  constructor(private route: ActivatedRoute
            , private router: Router) { 
    this.route.params.subscribe(params => {
      console.log(params);
      if(params != null || params.killes != null){
      this.score = params.killes 
      this.hero = params.pname
    }
    });
  }

  startGame(){
    if(this.hero == ""){
      this.hero = "Hero"
    }
    this.router.navigate(['./game',this.hero]);
  }
  
  ngOnInit() {
  }

}
